package com.puzzles;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Anagrams {

    // Time Complexity: O (n log n )
    public static List<List<String>> groupAnagrams(String[] strings) {
        if (strings == null || strings.length <= 0) {
            return null;
        }

        Map<String, List<String>> map = new HashMap<>();

        for (String string : strings) {

            char[] chars = string.toCharArray();
            Arrays.sort(chars);

            String key = String.valueOf(chars);
            if (map.containsKey(key)) {
                map.get(key).add(string);
            }
            else {
                List<String> stringList = new ArrayList<>();
                stringList.add(string);
                map.put(key, stringList);
            }
        }
        return new ArrayList<>(map.values());
    }

    // Time Complexity: O(n)
    public static boolean isValid(String string1, String string2) {
        if (string1.length() != string2.length()) {
            return false;
        }

        int[] alphabetArray = new int[26];

        for (int i = 0; i < string1.length(); i++) {
            alphabetArray[string1.charAt(i) - 'a']++;
            alphabetArray[string2.charAt(i) - 'a']--;
        }

        for (int i : alphabetArray) {
            if (i != 0) {
                return false;
            }
        }
        return true;
    }
}