package com.puzzles;

import org.junit.Test;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertTrue;

public class AnagramsTest {

    @Test
    public void isValid() {
        assertTrue(Anagrams.isValid("rat", "art"));
    }

    @Test
    public void groupAnagrams() {
        String[] strings = new String[]{"rat", "art", "cat", "act"};
        List<List<String>> groupedAnagrams = Anagrams.groupAnagrams(strings);
        System.out.println(groupedAnagrams);
        assertThat(groupedAnagrams.size(), is(2));
        assertThat(groupedAnagrams.get(0), contains("rat", "art"));
        assertThat(groupedAnagrams.get(1), contains("cat", "act"));
    }
}