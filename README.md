# Anagrams

This codebase meets the following two requirements:

1. Check if two strings are anagrams.

2. Groups anagrams together from an array of strings.

---
## Specification

---

### Check if Anagrams are Valid

For example, "apt", "tap" and "pat" are anagrams of each other.

Input two strings: 

```"apt"```
```tap```

Expected Output:

```true```

---

### Group Anagrams
Input (a String array):

```["rat", "art", "cat", "act"]```

Expected Output:

```
[[rat, art], [cat, act]]
```
---

### Acceptance Criteria

* Must meet the requirements mentioned in the spec.
* Must be testable via JUnit and provide full code coverage via a Maven build script.

---

### System Requirements

Need a computer with Java 1.8 (whether its the JDK or JRE) installed along with Apache Maven 3.3.3 set to run from the command line.

---

#### How to build and run the project

First way, is directly from command line, go to the root of the project and type:

`mvn clean install` (this runs the unit tests and will print out the results to stdout).